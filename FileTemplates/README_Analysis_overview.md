# Template of an analysis overview README file
Author: John Doe <br>
Creation date: 2020/04/28 (YYYY/MM/DD) <br>
Last modification: 2020/04/28 <br>

Example of an overview README file following this template: [README_convert_V3v4.md](../../convert_Ptrichocarpa_V3toV4/README_convert_V3v4.md)
<br>

**Comments:**
1. this template aims at providing a common README structure in order to make the reading easier to people that did not write the README files.
2. Of course, the format suggested here can be amended depending on your needs ut keeping a common structue will help collaborators et other scientists.
3. a markdown file such as this one can be converted into pdf or html and tools available to do so will depend on you OS (Windows, Mac, Linux) and IDE (Geany, atom, VisualStudio, Jupyter), so give a try to 'convert mardown to html' in yu favoured web search engine.

<br> From now onward, the current file describe the README template effectively.


## Summary (for readers in a hurry)
### Goal
Quick description of the goal of this analysis for readers in a hurry.

### Results
Quick description of the main results for readers in a hurry.

## Table of Content
<!-- TOC depthFrom:2 depthTo:5 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Summary (for readers in a hurry)](#summary-for-readers-in-a-hurry)
  - [Goal](#goal)
  - [Results](#results)
- [Table of Content](#table-of-content)
- [1. Goal](#1-goal)
- [2. Format of the result file(s) (optionnal)](#2-format-of-the-result-files-optionnal)
- [3. Step 1: Title (e.g. Study of epidemiology dynamics of SARS-CoV-2)](#3-step-1-title-eg-study-of-epidemiology-dynamics-of-sars-cov-2)
  - [3.1. Step 1.1: Title (e.g. Phylogenetic analysis of strains of SARS-CoV-2)](#31-step-11-title-eg-phylogenetic-analysis-of-strains-of-sars-cov-2)
    - [3.1.1. Strategy/methods](#311-strategymethods)
    - [3.1.2. Results](#312-results)
  - [3.2. step 1.2: epidemiology dynamics](#32-step-12-epidemiology-dynamics)
    - [3.2.1. strategy/methods](#321-strategymethods)
    - [3.2.2. Results](#322-results)
- [4. Step 2: Title](#4-step-2-title)
  - [4.1. step 2.1: Title](#41-step-21-title)
    - [4.1.1. strategy/methods](#411-strategymethods)
    - [4.1.2. Results](#412-results)
- [5. Synthesis (optionnal): can be combining results from steps 1 and step 2](#5-synthesis-optionnal-can-be-combining-results-from-steps-1-and-step-2)
- [6. programs and dependencies](#6-programs-and-dependencies)

<!-- /TOC -->

**Note:** the markdown table of contents can be automatically generated by add-ons in some IDE. For instance, in atom, you can use the add-ons markdown-toc or markdown writer.

_____________

## 1. Goal
More detailled descriptions of the goal of the analysis.

_____________


## 2. Format of the result file(s) (optionnal)
If needed, please describe the format in which the results are recorded (a link toward an website can do it). For example, we can have a list of genomic regions in bed format:

So let's describe the structure of a bed file:
<https://genome.ucsc.edu/FAQ/FAQformat.html#format1>

* the start position of a feature is 0-based, i.e. the first base of a chromosome is numbered 0.
* the end position of a feature is 1-based (the last position of a 100bp long contig is numbered 100)

**Note:** Another way to write a link in markdown: [bed file link 2] where the address of the link is described at the end of the file.


## 3. Step 1: Title (e.g. Study of epidemiology dynamics of SARS-CoV-2)
Described in folder [01_step1](../project_treeview_template/analysis_1/01_step1).
**Note:** There is no obligation to let the keyword "stepX" in the folder name, it's just to explain the rationnal.

### 3.1. Step 1.1: Title (e.g. Phylogenetic analysis of strains of SARS-CoV-2)
Described in folder [01_step1.1](../project_treeview_template/analysis_1/01_step1/01_step1.1).

#### 3.1.1. Strategy/methods
<u>**Strategy:**</u> title of the analysis, e.g.: "Maximum likelihood phylogeny of SARS-CoV-2 genomes".

* **Link to the main command file:** E.g. "**_All details of the analyses are given in_** the mardown file (Note: can be a script as well) [main_script_step1.1.Rmd](../project_treeview_template/analysis_1/01_step1/01_step1.1/main_script_step1.1.Rmd).
* **Link(s) to the initial data:** E.g. "The raw data (my_awesome_specific_data.gff.gz) can be found [here](../project_treeview_template/analysis_1/01_step1/01_step1.1/00_data/).
* **Some other specific data can be in other folders:** "The referene genome 'my_genome_reference.fasta.gz' can be found in [00_shared_data](../project_treeview_template/00_shared_data)".
* **Quick description of step 1.1:** Include description of substeps if any and with link to a relative scripts if any:
  - step 1.1.1: quick description
  - step 1.1.2: quick description (see script [check_results_step1.1.2.Rmd](../project_treeview_template/analysis_1/01_step1/01_step1.1/checking_results_step1.1.2/check_results_step1.1.2.Rmd))
  - ...

#### 3.1.2. Results
**Final result file:** [my_awesome_phylogenies.pdf](../project_treeview_template/analysis_1/01_step1/01_step1.1/01_results/my_awesome_phylogenies.pdf)

Quick description of the results here.


### 3.2. step 1.2: epidemiology dynamics
Described in folder [02_step1.2](../project_treeview_template/analysis_1/01_step1/02_step1.2).

#### 3.2.1. strategy/methods
Same structure as before, see section [3.1.1. Strategy/methods](#311-strategymethods)

#### 3.2.2. Results
Same structure as before, see section [3.1.2. Results](#312-results)


## 4. Step 2: Title
Described in folder [02_step2](../project_treeview_template/analysis_1/02_step2).
**Note:** There is no obligation to let the keyword "stepX" in the folder name, it's just to explain the rationnal.

### 4.1. step 2.1: Title
Described in folder [02_step2.1](../project_treeview_template/analysis_1/02_step2/01_step2.1).

#### 4.1.1. strategy/methods
Same structure as before, see section [3.1.1. Strategy/methods](#311-strategymethods)

#### 4.1.2. Results
Same structure as before, section [3.1.2. Results](#312-results)

## 5. Synthesis (optionnal): can be combining results from steps 1 and step 2
Quick description of the results and links to associated files.


## 6. Programs and dependencies
<font color="red"><b>This part is essential for reproducibility and creation of the singularity images, especially if analyses have been performed on a remote server (e.g. on a Galaxy server).</b></font>
Here must be listed the operating systems (OS) and informatic environments needed to perform the analysis:
- Environment/OS: windows, linux, job submission to a Galaxy server, ...
- programs and dependencies:
  - bash
  - GMAP version 2019-09-12
  - sort-bed 2.4.35
  - perl 5.30
  - gff2bed 2.4.35
  - BLAST xxx
  - R 3.6.3 (2020-02-29), with packages:
    - ggplot
    - X
    - Y
    - Z
  - Python 3.8.2, with packages:
    - ...
    - ...
  - anything else

[//]: # (link definitions)
[bed file link 2]: https://genome.ucsc.edu/FAQ/FAQformat.html#format1
