# Method for reproducibility and distribution
Author: Ludovic Duvaux <br>
Creation date: 2020/04/28 (YYYY/MM/DD) <br>
Last modification: 2020/06/05

## Summary
### Goal
This manual describes a method to allow the reproducibility and the protability of the EPITREE results and analyses. The method aims at allowing:

1. workflow sharing within the EPITREE group (i.e. scripts and methods).
2. the building of the singularity images that will be needed to allow reproducibility of the EPITREE analyses by the community.
3. the spread of the EPITREE methodologies and results in the community.


### Method: overview
Here is a summary of **the key points of the procedure**:

1. **Project organization** The EPITREE repository is organized by folders of main analyses/WP in order to make the browsing easy.
2. **Overall analysis description:** Each analysis folder include an **overview README file**, written in (R)markdown, describing succinctly the main steps of the analysis.
3. **Accessibility to analysis details:** The overview README file must link to important scripts (with meaningful code comments) or at the very least to files describing the details of the analyses (i.e. programs, parameter values, ...).
4. **Version control and synchronization:** Since several collaborators works on the EPITREE project, code and analyses must be synchronized on the forgemia EPITREE repository using the [Git] versioning program.

Note that code versioning using [Git] is also very convenient to transfer a workflows to a new location (e.g. to a high performance computing cluster, aka HPC) without hazardous copy-paste transfers.<br>


## Table of Content
<!-- TOC depthFrom:2 depthTo:5 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Summary](#summary)
  - [Goal](#goal)
  - [Method: overview](#method-overview)
- [Table of Content](#table-of-content)
- [1. Organization of the WP/analysis folders](#1-organization-of-the-wpanalysis-folders)
  - [1.1. Treeview](#11-treeview)
  - [1.2. Organization and key points](#12-organization-and-key-points)
- [2. Writing (R)mardown reports](#2-writing-rmardown-reports)
  - [2.1. Rational](#21-rational)
  - [2.2. Tutorial and documentation](#22-tutorial-and-documentation)
  - [2.3. Few useful html tags](#23-few-useful-html-tags)
  - [2.4. Which program to write/view md?](#24-which-program-to-writeview-md)
  - [2.5. How to convert md to html or pdf?](#25-how-to-convert-md-to-html-or-pdf)
- [3. Introduction to version control](#3-introduction-to-version-control)
  - [3.0. Version control: an overview](#30-version-control-an-overview)
  - [3.1. Using forgemia](#31-using-forgemia)
    - [3.1.1. What is a forge and why using forgemia?](#311-what-is-a-forge-and-why-using-forgemia)
    - [3.1.2. Connection to forgemia](#312-connection-to-forgemia)
    - [3.1.3. Connection to forgemia/GitLab by SSH keys](#313-connection-to-forgemiagitlab-by-ssh-keys)
  - [3.2. Getting started with Git and GitLab](#32-getting-started-with-git-and-gitlab)
    - [3.2.1. Installing Git](#321-installing-git)
    - [3.2.2. Tutorials](#322-tutorials)
  - [3.3. Initializing a project repository](#33-initializing-a-project-repository)
    - [3.3.1. Create the repository on forgemia](#331-create-the-repository-on-forgemia)
    - [3.3.2. Browse the forgemia/gitlab webpage](#332-browse-the-forgemiagitlab-webpage)
    - [3.3.3. Clone the repository on your computer](#333-clone-the-repository-on-your-computer)
  - [3.4. Version control: basic Git procedure](#34-version-control-basic-git-procedure)
    - [3.4.0. Overview](#340-overview)
      - [3.4.0.1. Difference between staging and committing](#3401-difference-between-staging-and-committing)
    - [3.4.1. Update your local repo](#341-update-your-local-repo)
      - [3.4.1.1. Git pull usage](#3411-git-pull-usage)
      - [3.4.1.2. What is a project 'branch'?](#3412-what-is-a-project-branch)
    - [3.4.2. modify your code/project locally](#342-modify-your-codeproject-locally)
    - [3.4.3. Check, stage and commit changes](#343-check-stage-and-commit-changes)
      - [3.4.3.1. Rational](#3431-rational)
      - [3.4.3.2. Check changes (optional)](#3432-check-changes-optional)
      - [3.4.3.3. Stage and commit changes](#3433-stage-and-commit-changes)
    - [3.4.4. Upload the commited changes to a remote server](#344-upload-the-commited-changes-to-a-remote-server)
    - [3.4.5. Example using the .gitignore file](#345-example-using-the-gitignore-file)
  - [3.5. Version control: Worflow for collaborative project](#35-version-control-worflow-for-collaborative-project)
    - [3.5.0. Workflow overview](#350-workflow-overview)
    - [3.5.1. Update your local repo](#351-update-your-local-repo)
    - [3.5.2. Create a working branch](#352-create-a-working-branch)
    - [3.5.3. Make your changes](#353-make-your-changes)
    - [3.5.4. stage & commit on the new branch](#354-stage--commit-on-the-new-branch)
    - [3.5.5. Push on the new branch](#355-push-on-the-new-branch)
    - [3.5.6. Request branch merging](#356-request-branch-merging)
  - [3.6 Using issues on gitlab](#36-using-issues-on-gitlab)
  - [3.7 Version control: IDE integration or GUI clients for git.](#37-version-control-ide-integration-or-gui-clients-for-git)

<!-- /TOC -->
_______________

## 1. Organization of the WP/analysis folders

It is optional but **highly recommended** to adopt a common tree view / organization for all analysis/WP folders as it would make the browsing easier for anybody.

### 1.1. Treeview
If possible, I highly recommend to adopt the following **_global_** structure for the analysis folders:<br>


<a name="fig1">Figure 1</a>. Treeview of the suggested project structure.
```
project_treeview_template/
├── shared_data
│   └── my_genome_reference.fasta.gz
├── utils
│   └── my_general_purpose_script.py
├── analysis_1
│   ├── 01_step1
│   │   ├── 01_step1.1
│   │   │   ├── 00_data
│   │   │   │   └── my_awesome_specific_data.gff.gz
│   │   │   ├── 01_results
│   │   │   │   └── my_awesome_phylogenies.pdf
│   │   │   ├── checking_results_step1.1.2
│   │   │   │   └── check_results_step1.1.2.Rmd
│   │   │   └── main_script_step1.1.Rmd
│   │   └── 02_step1.2
│   │       ├── 00_data
│   │       │   └── list_data_files.txt
│   │       ├── 01_results
│   │       └── Description_step1.2.md
│   ├── 02_step2
│   │   ├── 00_data
│   │   ├── 0.0_utils
│   │   │   └── get_stats.R
│   │   ├── 01_results
│   │   └── main_script_step2.sh
│   └── README_Analysis1_overview.md
└── analysis_2
    ├── 01_step1
    ├── 02_step2
    └── README_Analysis2_Overview.Rmd
```


### 1.2. Organization and key points

1. **One folder per WP or main analysis** (e.g. `analysis_1` on <a href="#fig1">fig.1</a>):
   - template ([link](./project_treeview_template/analysis_1)) & example ([convert_Ptrichocarpa_V3toV4/](../convert_Ptrichocarpa_V3toV4/)).
2. **Each root analysis folder** (i.e. `analysis_1/` and `analysis_2/`on <a href="#fig1">fig.1</a>):
   - <u>**MUST**</u> include an <font color="red"><b>analysis overview README file</b></font>. This file aims at **describing the analysis** globally (not much details) and **to list, using hypertext links,** the main scripts of the WP/analysis (since the scripts include the details of the analysis).
      - template of the README file with exhaustive description ([link](./FileTemplates/README_Analysis_overview.md)) & example ([README_convert_V3v4.md](../convert_Ptrichocarpa_V3toV4/README_convert_V3v4.md)).
      - **may** include several folders and sub-folders of sub-analyses:
        - templates ([here](./project_treeview_template/analysis_1/02_step2) and [here](./project_treeview_template/analysis_1/01_step1/01_step1.1)) & examples of sub-analyses ([here](../convert_Ptrichocarpa_V3toV4/01_sans_a_priori/) and [here](../convert_Ptrichocarpa_V3toV4/02_candidats/03_DoubleChecking_BLAST-Maury/)).
3. **Each tip analysis folder** (e.g. `01_step1.1` or `02_step2` on <a href="#fig1">fig.1</a>):
   - **must** include a **[folder with the initial data](./project_treeview_template/analysis_1/01_step1/01_step1.1/00_data)** (or symlinks or a text file listing all the relevant data files). E.g. `00_data` folders on <a href="#fig1">fig.1</a>.
   - **must** include a folder including the **[main results](./project_treeview_template/analysis_1/01_step1/01_step1.1/01_results)** (e.g. `01_results` folders on <a href="#fig1">fig.1</a>).
   - **must** include **the main scripts** used for the analysis (e.g. `main_script_step1.1.Rmd` or `main_script_step2.sh` on <a href="#fig1">fig.1</a>).
      - See examples of [bash scripts](./project_treeview_template/analysis_1/02_step2/main_script_step2.sh) and [md or Rmd](./project_treeview_template/analysis_1/01_step1/01_step1.1/main_script_step1.1.Rmd).
      - <font color="red"><u>If no script is available</u></font>, **describe the details of the analysis** in a dedicated [markdown file](./project_treeview_template/analysis_1/01_step1/02_step1.2/Description_step1.2.md) (Galaxy server? Program(s)? Parameter values? and so on). These files have the same format as the [analysis overview template](./FileTemplates/README_Analysis_overview.md).
   - **may** include a folder `0.0_utils` including *ad hoc* scripts and executables that will be ran by some main scripts. E.g.`get_stats.R`  on <a href="#fig1">fig.1</a> ([link](./project_treeview_template/analysis_1/02_step2/0.0_utils/get_stats.R)).
   - **may** include supplementary folders depending on requirements (e.g. ["checking_results_step1.1.2"](./project_treeview_template/analysis_1/01_step1/01_step1.1/checking_results_step1.1.2) on <a href="#fig1">fig.1</a>). This folder can include:
      - supplementary quick and dirty analyses and tests
      - intermediary results to be kept but that the user to want to mix with the main results
      - specific data files one want to find easily (see [03_DoubleChecking_BLAST-Maury/02_bedfiles](../convert_Ptrichocarpa_V3toV4/02_candidats/03_DoubleChecking_BLAST-Maury/02_bedfiles) for a real example).
4. **Finally, the root project folder** (`project_treeview_template/` on <a href="#fig1">fig.1</a>):
   - **may** include a folder `shared_data` for data common to several analyses (e.g. genome sequence like [Ptrichocarpa_533_v4.0.fa.gz](../../00_shared_data/Ptrichocarpa_533_v4.0.fa.gz)).
   - **may** include a folder `utils` for scripts and executables common to several analyses.
_______________

## 2. Writing (R)mardown reports
### 2.1. Rational
<font color="red"><b>The above organization heavily relies on writing reports</b></font>. I suggest to use **(R)markdown** to do so, **a very powerful yet quick and simple** way to write reports than can easily be converted to html or pdf files.<br>

The present file is a pretty honest markdown exemple (README_Analysis_overview.md). See also the template of the analysis overview md file with exhaustive description ([link](./FileTemplates/README_Analysis_overview.md)) & an actual example ([README_convert_V3v4.md](../convert_Ptrichocarpa_V3toV4/README_convert_V3v4.md)).

### 2.2. Tutorial and documentation
Here are a couple of good web pages to quickly learn about md **(~15min to learn the basics)**:

* English
  * <https://amelon.org/2018/03/13/md-reference.html>
  * <https://www.markdownguide.org/getting-started/>
* French
  * <https://www.christopheducamp.com/2014/09/18/love-markdown/>
  * <https://blog.wax-o.com/2014/04/tutoriel-un-guide-pour-bien-commencer-avec-markdown/>
  * <https://support.zendesk.com/hc/fr/articles/203691016-Formatage-de-texte-avec-Markdown#topic_xqx_mvc_43__row_k3l_yln_1n>
  * <https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown>


### 2.3. Few useful html tags

```
<b>bold</b>
<u>underlined</u>
<a name="anchor">My anchor</a>
<a href="#anchor">if I click here, I'll go back to the position of 'My anchor'</a>
<font color="red">Get some red color</font>
<p>paragraph (allow to add a blank line)<p>
line<br>break
```

* <b>bold</b><br>
* <u>underlined</u><br>
* <a name="anchor">My anchor</a><br>
* <a href="#anchor">if I click here, I'll go back to the position of 'My anchor'</a><br>
* <font color="red">Get some red color</font><br>
* <p>paragraph<p>
* line<br>break

Difference between `<br>` and `<p>` tags (french): <https://forum.alsacreations.com/topic-2-8421-1-Resolu-Quand-utiliser-ltbr-gt-et-ltpgt.html>

* `<br>` is a mere line break, useful to force the interpreter to go back to next line (e.g. useful to force new lines in lists).
* `<p>` is more structuring: creates margins, allows indent and so on...


### 2.4. Which program to write/view md?
Some programs or websites are dedicated:

* [Remarkable](https://remarkableapp.github.io/)
* <https://dillinger.io/>


 However most [IDE]s have add-ons allowing to interpret Markdown:

* Rstudio
* geany <https://plugins.geany.org/markdown.html>
* jupyter
* atom ([website](https://atom.io/) and [tutorial](https://jstaf.github.io/2018/03/25/atom-ide.html)) with packages:
  - markdown writer
  - markdown TOC
  - markdown preview
  - markdown pdf
  - markdown themeable pdf
* visual studio code (not-at-all-subjective-disclaimer: take care, that's a Micro$oft program...)

Most programs are usually able to interpret html tags what is pretty convenient (see [2.3. Few useful html tags](#23-few-useful-html-tags)).

### 2.5. How to convert md to html or pdf?
Again, there's plenty of website that can do that (never tried though). See here: <https://zapier.com/blog/markdown-html-export/> or ask google ("markdown to html" "markdown to pdf", add "convert" if neded).<br>

* <https://dillinger.io/>
* <https://www.markdowntopdf.com/>

If you prefer a dedicated local program, you can use [ReText](https://framalibre.org/content/retext).<br>

If you are more adventurous (or want to work with an efficient [IDE]), many [IDE]s have extension to do so:

* atom: markdown to pdf
* atom: markdown themeable pdf
* visual studio code: see [here](https://code.visualstudio.com/docs/languages/markdown) and [copy-markdown-as-html](https://marketplace.visualstudio.com/items?itemName=jerriepelser.copy-markdown-as-html)
_______________

## 3. Introduction to version control
### 3.0. Version control: an overview
Version control aims at recording and synchronizing all versions of an informatic project (code) and text reports written (record) by several collaborators (synchronization).<br>

To perform code versioning, one needs:

* a versioning program (e.g. cvs, svn, mercurial (hg), bazaar (bzr)). Here, we will use [Git].
* a remote server where to save/synchronize our project (repository). Here, we will synchronize our repo on [forgemia].
* (optional but making life easier): an [IDE] integrating natively [Git] version control (e.g. Atom, Visual Code Studio, ...).

Note that code versioning using [Git] is also very convenient to transfer a workflow to a new location (e.g. to a high performance computing cluster, aka HPC) without hazardous copy-past transfers.<br>

Last but not the least, git keep tracks of **ALL** versions of the code you kept track withh it. Therefore, in case of trouble it is possible to revert to a past state where the code was functional (case not described in this document).


### 3.1. Using forgemia
#### 3.1.1. What is a forge and why using forgemia?
A forge is is a web-based collaborative software platform for both developing and sharing computer applications ([wiki def here](https://en.wikipedia.org/wiki/Forge_(software))).<br>

Actually there is no obligation for the git remote server to be a online forge. One can very well set up the git synchronization on a local intranet server (or even maybe on an external HDD).

However, some specialized forge like [GitHub](https://github.com/), [Bitbucket](https://bitbucket.org/) or [GitLab] offers pretty convenient repository and user group management tools. Furthermore, online forges make collaborative synchronization much easier.

For security and sovereignty reasons, any INRAE project _**should**_ be hosted on [forgemia], the forge created and maintained by the INRAE MIA department. This forge, hosted by and thus benefiting from all [GitLab] tools, is accessible to many French academics (no idea so far for international collaborators (2020/05/05)).


#### 3.1.2. Connection to forgemia

In order to connect to forgemia:

* go to <https://forgemia.inra.fr/>
* sign in using the `connexion SSO` button
* you should be redirected to a Renater portal (not sure if the direct link works but hey, [just try](https://discovery.renater.fr/renater/?entityID=https%3A%2F%2Fforgemia.inra.fr%2Fsp&return=https%3A%2F%2Fforgemia.inra.fr%2FShibboleth.sso%2FLogin%3FSAMLDS%3D1%26target%3Dss%253Amem%253A3c1ecdc8052ce18ebfdda397430ed8f48e63c63575660c728bded4cff3176c63))
* select your organisation (e.g. INRA (not yet INRAE on 2020/May/04), University of Tours, ...)
* the first time you connect, take some time to fill in the account and settings information


#### 3.1.3. Connection to forgemia/GitLab by SSH keys
Setting the SSH key is not compulsory but that's make life so much easier that I strongly **recommend** you to set it up:

* no more password needed for synchronization
* some linux services only available through SSH: E.g. [nautilus](https://doc.ubuntu-fr.org/nautilus) is really great to browse remote servers.

SSH is a strong asymetrical encryption protocol based on a pair of keys: the public key and the private key.

* **the public key** is the key sent to remote servers or shared with other computers. Anybody can read the public key as it can only **ENCRYPT** the information to be sent. Indeed, since the protocol is asymetrical, the public key **CANNOT decrypt** an encrypted piece of information.
* **the private key** on the other hand is used to **DECRYPT** information **encypted with its associated public key ONLY** (i.e. a private key from a pair of keys A cannot decrypt information encrypted by a public Key of a pair B). **Therefore:**

<font color="red"><b><u> WARNING:</b></u></font> **Never, ever share your private key**. Only the public key should be shared with a remote server or another computer. If you mistakenly do so, erase your SSH folder (`~/.ssh/` on linux) and create a new pair (meaning that yes, you will have to share your public key again with all the servers you want to communicate with).

Here are some tutorials:

* FR:
  - [openclassroom SSH tutorial](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/41773-la-connexion-securisee-a-distance-avec-ssh)
  - [GitLab SSH key tutorial (FR)](https://github.com/SocialGouv/tutoriel-gitlab#gestion-des-fichiers): scroll down until the section `Clé SSH`
* EN:
  - [GitLab SSH key tutorial (EN)](https://docs.gitlab.com/ee/ssh/)


### 3.2. Getting started with Git and GitLab
#### 3.2.1. Installing Git

On linux, use your favorite package manager (e.g. apt, pacman, YUM, ...).
Alternatively, Windows/Mac users can download the executable on [git website](https://git-scm.com/downloads).

In any case, you may find useful the following links:

* instructions on [linode website](https://www.linode.com/docs/development/version-control/how-to-install-git-on-linux-mac-and-windows/).
* instructions on [bitbucket (FR)](https://www.atlassian.com/fr/git/tutorials/install-git)
* instructions on [GitLab (EN)](https://docs.gitlab.com/ee/topics/git/how_to_install_git/index.html)


#### 3.2.2. Tutorials
**NOTE:** Some tutorials presented below will describe how to set up a [github](https://github.com/) or a [bitbucket](https://bitbucket.org/) account. Please, <u>**do ignore**</u> these steps. Instead, be sure you have an [active account on forgemia](#312-connection-to-forgemia)

I describe the procedure of git versionning in the following sections but if you need more information, here are few tutorials to get started:

* Git tutorials:
  - David Parsons' (INRIA) tutorial: <https://sed.inrialpes.fr/Presentations/180528-GitIntro/slides.pdf>
  - Happy Git With R: <https://happygitwithr.com/>
  - Software Carpentry Git tutorial:
    - <http://swcarpentry.github.io/git-novice/>
    - <https://rr-france.github.io/bookrr/C-versioning.html#ref-softwarecarpentryVersionControlGit>
  - git pro (the two first chapter are enough to get started):
    - FR: <https://git-scm.com/book/fr/v2>
    - EN: <https://git-scm.com/book/en/v2>
  - on [openclassrooms](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git) (FR)
  - <https://www.hostinger.fr/tutoriels/tuto-git/> (FR)
  - learn git branching: <https://learngitbranching.js.org/>
  - on [GitLab](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) (EN)
  - [University of Toronto tuto](https://uoftcoders.github.io/studyGroup/lessons/git/collaboration/lesson/) (EN)
  - [opensource tuto] (EN)
  - [towardsdatascience tuto](https://towardsdatascience.com/getting-started-with-git-and-github-6fcd0f2d4ac6) (EN)
  - chapter in "Vers une recherche reproductible" <https://rr-france.github.io/bookrr/C-versioning.html#ref-chaconProGitTout2019>
* GitLab tutorials:
  - on [github](https://github.com/SocialGouv/tutoriel-gitlab) (FR)


### 3.3. Initializing a project repository

In the case of the EPITREE project, a repository already exists on GitLab ([EPITREE GitLab]), thus you don't have to create a repo. I describe the method anayway in the case you have to create a repository on [GitLab] eventually.

#### 3.3.1. Create the repository on forgemia

1. Log to [forgemia] (see [section 3.1.2](#312-connection-to-forgemia))
2. click on `new project`
3. on the settings page:
   1. choose a project name
   2. add users or groups of users to the project: typically for a collaboration, one wants to set up a user group (e.g. EPITREE-team, see [user group webpage](https://docs.gitlab.com/ee/user/group/#create-a-new-group)). All members of this group will automatically be granted permissions on the repo (according to the settings of the user groups).
   3. choose a visibility level: usually, most academic unpublished projects are aimed to be "private" repositories (not publicly accessible projects). But if the project is totally free you can set it "public".
4. you can initiate the project with the general README in md format (see [1.2. Organization and key points](#12-organization-and-key-points)).
5. Now you can [clone this online repo on your local computer](#333-clone-the-repository-on-your-computer)

**NOTE**: Another way to create a git project is to first create the repo locally on your computer. It's not complicated *per se* but usually it makes the connection to the GitLab repo more complex afterwards (and you need to create the repo on GitLab anyway). Therefore I usually prefer and recommend the 'create online and clone' method presented above. However, if you want to try the 'create the repo locally' method, here's an [opensource tuto].


#### 3.3.2. Browse the forgemia/gitlab webpage
Description of the most useful tabs of the gitlab webpage.

* `Project overview`
  - `Details`: show the repo structure and the content of the main README.
  - `Activity`: show changes (commits, issue resolution, ...)
* `Repository`
  - `Files` -> allow to browse any version of a given file by clicking the file and then the `History` button (topright). Convenient if we have lost a file in the current state (HEAD) of the repo.
  - `Commits`: commit list.
  - `Graph`: show relation of the different commits on the different branches.
* `Issues`: page to set and manage issues.
* `Merge requests`: page to ask the maintanier(s) to merge branches.
* `Wiki`: section to generate wiki documentation.
* `Settings`: miscellaneous settings options and list of members.


#### 3.3.3. Clone the repository on your computer
Ok, you've got your repo ready on [GitLab] and now you want to transfer it on your computer. This step is called 'cloning' the repo.<br>
On your computer, browse to the folder you want to clone the EPITREE repo in (e.g. in your 'all_my_analyses_ever' folder), then:

```bash
# if you have added your **PUBLIC** SSH key on GitLab
git clone git@forgemia.inra.fr:epitree-team/epitree.git

# if not
git clone https://forgemia.inra.fr/epitree-team/epitree.git
<type your password (LDAP for INRAE agents)>
```
At this point you can create a `.gitignore` file at the root of the cloned folder ([see this section](#345-example-using-the-gitignore-file)). But before that, let's describe the Git workflow.

**NOTE**: I never checked but cloning the repo, means fetching all the repo history as well. So, it should be possible downgrade the cloned code to any previous version present in the git history.

### 3.4. Version control: basic Git procedure
Here, I explain the basic git procedure.<br>
It is explained as an introduction to the git rational but this basic workflow won't be used within the EPITREE project. If you are already familiar with git, you can directly go to [section 3.5](#35-version-control-worflow-for-collaborative-project).


#### 3.4.0. Overview

1. update your local repo with `git pull <remote> <branch>` (see [3.4.1](#341-update-your-local-repo))
2. modify your code/project locally ([3.4.2](#342-modify-your-codeproject-locally))
3. (optional) check your changes with `git status` and `git diff` ([3.4.3](#343-check-stage-and-commit-changes))
4. stage and commit your changes with `git add` and `git commit -m "description of your commit"` ([3.4.3](#343-check-stage-and-commit-changes))
5. upload the changes to the remote server with `git push` ([3.4.4](#344-upload-the-commited-changes-to-a-remote-server))

##### 3.4.0.1. Difference between staging and committing
* Staging means "collecting a set of changes to be commited"
* committing means "recording the staged changes in the git history"

Having these two steps separated allows to very finely control the code history (very useful for professionnal developpers). However, in pratice we will most often stage and commit the changes we made during a working session all at once (see [here](https://community.atlassian.com/t5/Bitbucket-questions/what-is-the-difference-between-staging-area-and-repository/qaq-p/430758) for more information).

More precisely, git manages three versions of the code
```
« past »    : The last comitted version we are working from (Version)
« current » : The current content of our working directory (WD)
« future »  : The version we have asked git to follow/stage     (Index)

 →  WD -- git add <file>   ->   Index (staging area) →
↑                                                     ↓
 ← WD <-  git reset fichier -- Index (staging area)  ←

 → Index (staging area)        -- git  commit ->          Version (HEAD) →
↑                                                                         ↓
 ← Index (staging area)  <- git reset --soft HEAD~ --     Version (HEAD) ←

WD <- git reset --hard HEAD~  --   Version (HEAD) →
↑                                                  ↓
 ←               ←               ←                ←
```


#### 3.4.1. Update your local repo
##### 3.4.1.1. Git pull
Always start a programming session by updating your local project repo. If several persons work simultaneously on the same project, the code may very well have changed since your last session. Update it!<br>

Here the main command:
```bash
# generic command:
    # git pull <remote> <branch>
git pull origin master
```
- `git pull` is the command to update your local repo from a remote server
- `<remote>`: a project may be synchronized on several remote servers (gitlab, github, bitbucket, a local server...). This can be useful for backup purpose for example. Therefore, the user must specify the name of the remote to pull the code from.
- Usually the main remote is called `origin` but it's not an obligation. For example, the maintaner of the EPITREE repo chose **to rename its default remote from `origin` to `forgemia`**.
- `<branch>`: a git project usually has several branches (see defintion in [next section](#3412-what-is-a-project-branch) or on the [official webpage](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)).

Since it is not convenient to always specify the remote where to push the code on, we can set, for each branch, a default remote using the commands:
```bash
# method 1: just setting the default remote for the branch <branch>
   # git branch --set-upstream-to <remote>/<branch>
git branch --set-upstream-to origin/master
   # add another remote
git remote add github git@github.com:<login>/<repo>.git
# method 2: set the default remote and push a commit simultaneously
  # git push --set-upstream <remote> <branch>
git push --set-upstream origin master
# after that just:
git push
```

**IMPORTANT NOTE:** Git pull is actually a "shortcut" for the successive two commands:
```bash
git fetch
git merge
```
In the simplest terms, git pull does a git fetch followed by a git merge.

`git fetch` never changes any of your own local branches under refs/heads, and is safe to do without changing your working copy. **Meaning you can run a `git fetch` even if your working directory is not clean (not commited changes).**

A `git pull` is what you would do to bring a local branch up-to-date with its remote version, while also updating your other remote-tracking branches. **It's important to always have a clean working directory (any change commited) before a `git pull`**.


##### 3.4.1.2. What is a project 'branch'?
Basically, branches are parallel versions of the project:
<https://fr.lutece.paris.fr/fr/jsp/site/Portal.jsp?page=wiki&view=page&page_name=git>

Branches can be merged eventually when keeping them separated is not required anymore.<br> Branches may have different usages, here are the most common ones:

- **the main branch:** the main "development line". Usually called `master`.
- **dev branches:** a branch where important developments are made. Can be called anything, e.g. `new_awesome_feature`.
- **patch branches:** useful to implement a correction patch without messing with the main branch. When the patch is deemed functional, such a branch is typically merged onto the main branch and does not exist per itself any longer. If needed, git allow to revert to a version of the code where the two branches was still distinct (case not described in this document)
- **release branches:** when a version of the code is deemed stable enough, a good practice can be to create a branch that won't be modified anymore and to distribute this code as an executable.

#### 3.4.2. modify your code/project locally
Here's the "easy" part. Do whatever you want to your project: add code, remove files, change the organization...

#### 3.4.3. Check, stage and commit changes
##### 3.4.3.1. Rational
The official description of these steps is here:
<https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository>

As explained in the section ['Difference between staging and committing'](#3401-difference-between-staging-and-committing), Git permits to very finely control the versioning by allowing to stage bits of code independently of their creation time. Here's an illustration:

Changes to the repo:

* add or change code to file 1 at lines 10-15, 22, 43
* create a new file 'new_file'
* add code to file 2 at lines 50-122
* remove file 'bad_file'

One could perform versioning as follow:

 1. stage the modification of line 22 of file 1
 2. commit the staged changes
 3. tell git to track  and stage the file 'new_file'
 4. tell git to untrack the file 'bad_file' that does not exist anymore
 5. commit staged changes
 6. stage all remaining changes
 7. commit all remaining changes


Here, for simplicity **I will only describe the versioning of all changes simultaneously**. Such a process is quite simple and linear:

 1. make changes to code or files of the repo
 2. stage and commit all changes
 3. come back to 1.

##### 3.4.3.2. Check changes (optional)
You can start by checking your changes `git status`
<https://git-scm.com/docs/git-status>
<https://www.atlassian.com/fr/git/tutorials/inspecting-a-repository>
```
[ludo@garuda test_project]$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	test.txt

no changes added to commit (use "git add" and/or "git commit -a")
```
You can also check the details of this changes with `git diff`
<https://git-scm.com/docs/git-diff>
<https://www.atlassian.com/fr/git/tutorials/saving-changes/git-diff>
```
[ludo@garuda test_project]$ git diff
diff --git a/.gitignore b/.gitignore
index e150297..1a61518 100755
--- a/.gitignore
+++ b/.gitignore
@@ -45,7 +45,7 @@
 !*.rmd
 !*.sh
 #!*.template
-!.txt
+!*.txt

 # 3) add a pattern to track the file patterns of section2 even if they are in
     # subdirectories

```

##### 3.4.3.3. Stage and commit changes
Here are the main commands to record your current code version:
```bash
# stage all changes and track all new files
git add . --all
# commit all staged changes
git commit -m "description of my modifications"
```

<font color="red"><b>IMPORTANTE NOTE:</b></font> Eventually, you will find that some files are not tracked by Git. **It is most likely NORMAL** and just means that these files have been "blacklisted" by the infamous <a href="#gitignore_def">.gitignore</a> file!<br>

If you need to allow new extensions to be tracked by git, please set an issue (see <a href="#set_issue">here</a>).


#### 3.4.4. Upload the commited changes to a remote server
To this point, changes have been staged and committed but they still need to be synchronized on the remote server. Just type:

```Bash
git push
```

#### 3.4.5. Example using the .gitignore file
At the very beginning of a project, it is advised to set a `.gitignore` file.<br>

<https://docs.microsoft.com/en-us/azure/devops/repos/git/ignore-files?view=azure-devops&tabs=visual-studio>

The rational of version control is to keep tracks of code versions only. Therefore, only code/txt files are expected to be tracked by git and any other files (data, temp files, Rdata files and so on) are usually untracked.<br>

<a name="gitignore_def">The `.gitignore` file</a> (hidden file on a Unix OS) aims at doing so efficiently by listing all file patterns (e.g. file extensions, full file names, full file path) we don't want to track. As suggested by its name, the gitignore **usually works as a black list** (i.e. anything listed in it won't be tracked).<br>

Alternatively, a more secure way to track only relevant files is **to set the .gitgnore as a white list** (i.e. only listed file patterns are tracked). Here's a [.gitignore template](./FileTemplates/.gitignore).<br>

<font color="red"><b>IMPORTANT NOTE:</b></font> don't forget that any modification to the gitignore file will impact the whole repository, that is even folders not created and maintained by yourself. So if you want to modify it, it's better do it specifically for the analysis folder you created (see template and example).

So to modify the `.gitignore`, use your prefered text editor or IDE and then:

```Bash
git add .gitignore
git commit -m "modification of the gitignore file: tracks text file"
git push forgemia master

```

### 3.5. Version control: Worflow for collaborative project
For several people to collaborate properly on the same project, the following [workflow is suggested](https://uoftcoders.github.io/studyGroup/lessons/git/collaboration/lesson/).<br>

Thsi require to manage branches:
<https://fr.lutece.paris.fr/fr/jsp/site/Portal.jsp?page=wiki&view=page&page_name=git>


#### 3.5.0. Workflow overview

1. update your local repo with `git pull <remote> <branch>`
2. (optionnal) list all project branches `git branch -a`
3. create and switch to a new branch `git checkout -b <branch>`
4. make your changes and stage them with `git add . --all`
5. commit your changes to your branch with `git commit -m "description of your commit"`
6. upload the changes (including your new branch) to the remote server with `git push <remote> <branch>`
7. you can switch between branches using `git checkout <branch>`
8. When your done with the **MAIN** changes, ask the repo maintainer (ie Ludovic) to merge you branch with the master branch.

#### 3.5.1. Update your local repo
Before to start, one should be sure to have the last version of the repo with the very last commited changes. To do so, the generic command is `git pull <remote server> <branch>` that is in our case

```bash
git pull forgemia master
```

#### 3.5.2. Create a working branch
Strictly speaking, there is no obligation to create a new branch <u>**HOWEVER**</u> this is strongly recommended in case something may go wrong (and when one starts using a new tool, there are plenty of reasons something can possibly go wrong).

The main idea behind creating a new branch is that the new branch is totally independent from the main branch (usually called the `master` branch). Thus, one can do whatever he/she wants on the new branch, even breaking the whole repo as we can easily fix it with the `master` branch.

Users can very easily shift from one branch to another and perform independent changes in each of them (take care not to be lost between branches like "Oh no I was not on my test branch and I messed up with someone else code...").

See a pedagogic main doc [here](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) and a tutorials:

* [Kunena github](https://github.com/Kunena/Kunena-Forum/wiki/Create-a-new-branch-with-git-and-manage-branches)
* [www.freecodecamp.org](https://www.freecodecamp.org/forum/t/push-a-new-local-branch-to-a-remote-git-repository-and-track-it-too/13222)


And the main commands below:

```bash
# create THEN switch to a new branch 'MyBranch'
git branch MyBranch
git checkout MyBranch
# create AND switch to a new branch 'MyBranch'
git checkout -b MyBranch
# Specify the 'forgemia' remote for MyBranch (two possibilities)
git branch --set-upstream-to forgemia/folders4WP
git push --set-upstream forgemia folders4WP
# check all the created branches
git branch -a
# switch back to the master branch
git checkout master
```


#### 3.5.3. Make your changes
Once you are on the new branch, you can start to make your changes.
Basically, in the EPITREE project the work flow would be like this:

1. go to the repo of the project on your laptop
2. switch to your branch
3. create a directory `analysis_whatever` for the analysis you were in charge
4. make your changes into the directory `analysis_whatever`
   * if the analysis was done before you use git, just copy-paste the files in the directory `analysis_whatever`
   * if not done yet, just code in this directory as usual.
5. then stage and commit (see next section).

<font color="red"><b>IMPORTANT NOTE</b></font>: as long as it has not committed, a new feature (a new file or a code modification) will appear in all branches on your laptop (create the file "NewFile.txt" and switch between branches: you could see the new file in all branches). It's normal as you haven't said to git yet what it should do of this new feature. However, as soon as you have staged and committed it, the new feature will only appear in the branch in which it has been committed. That's the magic of staging/committing!


#### 3.5.4. stage & commit on the new branch

```bash
# stage and commit
git add . --all
git commit -m "My changes on my awesome new branch"
```
Importantly, it is the `git commit` command that specify on which branch a change is recorded. If you stage your changes on branch A but commit the changes on branch B, the changes will be associated to branch B!


#### 3.5.5. Push on the new branch
If you haven't done so before (see [section 'create a working branch'](#352-create-a-working-branch)), you'll have to specify the remote branch where to push the commit:

```bash
# push it on forgemia
	# generic command
git push --set-upstream <remote> <MyNewBranch>
git push -u <remote> <MyNewBranch>
	# example command
git push -u forgemia MyNewBranch

# any commit afterwards may be pushed by a mere
git push
```

#### 3.5.6. Request branch merging
When you're done with your main changes, you can ask the maintainer (aka Ludovic Duvaux) to merge your branch onto the master branch.

To do so, go to:
<https://forgemia.inra.fr/epitree-team/epitree/-/merge_requests>

Specify:

* the source (usually your branch)
* the target (usually the master branch)
* you can add other comments if needed. Please see the documentation if needed.

After the merging, subsequent changes can be made:
* directly on the master branch if it is small changes (see [section 3.4](#34-version-control-basic-git-procedure)).
* by creating another branch if there are a lot of changes

For the moment we will stick with this procedure in case mergings may be complicated. If it's too much work for me or if the mergings always go smooth, then I'll allow other people to merge branches.


### 3.6 Using issues on gitlab
You have a problem with the workflow or with this documentation? Please use the <a name="set_issue">gitlab issue</a> system to notify it.<br>

<https://forgemia.inra.fr/epitree-team/epitree/-/issues>

This has several advantages:

* easy tracking system
* allow to set priorities
* allow to keep archives

### 3.7 Version control: IDE integration or GUI clients for git.
Some IDE (e.g. Atom or visual code studio) allow to perform staging, committing and pushing very easily using a graphical interface.

Some GUI clients also exist to do versionning using a graphical interface:
<https://git-scm.com/download/gui/linux>.

I didn't do it myself so I let you check for that.



[//]: # (link definitions)
[GitLab]: https://gitlab.com/
[forgemia]: https://forgemia.inra.fr/
[opensource tuto]: https://opensource.com/article/18/1/step-step-guide-git
[.gitignore template]: ./FileTemplates/.gitignore
[EPITREE .gitignore]: ../../.gitignore
[EPITREE GitLab]: https://forgemia.inra.fr/epitree-team/epitree
[IDE]: https://en.wikipedia.org/wiki/Integrated_development_environment
[Git]: https://git-scm.com/
