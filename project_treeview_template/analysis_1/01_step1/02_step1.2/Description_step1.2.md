# Template of an analysis overview readme file
Author: John Doe <p>
Creation date: 2020/04/28 (YYYY/MM/DD) <p>
Last modification: 2020/04/28 <p>

## Summary (for readers in a hurry)
No scrit is availablefor this analysis so I describe the analysis with much details here.


### Goal
Quick description of the goal of this analysis for readers in a hurry.

### Results
Quick description of the main results readers in a hurry.

## Summary (for readers in a hurry)
### Goal
Quick description of the goal of this analysis for readers in a hurry.

### Results
Quick description of the main results readers in a hurry.

## Table of Content
<!-- TOC depthFrom:2 depthTo:5 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Summary (for readers in a hurry)](#summary-for-readers-in-a-hurry)
  - [Goal](#goal)
  - [Results](#results)
- [Summary (for readers in a hurry)](#summary-for-readers-in-a-hurry-1)
  - [Goal](#goal-1)
  - [Results](#results-1)
- [Table of Content](#table-of-content)
- [1. Goal](#1-goal)
- [2. Format of the result file(s) (optionnal)](#2-format-of-the-result-files-optionnal)
- [3. Step 1](#3-step-1)
  - [3.1. Step 1.1](#31-step-11)
    - [3.1.1. Strategy/methods](#311-strategymethods)
    - [3.1.2. Results](#312-results)
  - [3.2. step 1.2: epidemiology dynamics](#32-step-12-epidemiology-dynamics)
    - [3.2.1. strategy/methods](#321-strategymethods)
    - [3.2.2. Results](#322-results)
- [4. Step 2: Title](#4-step-2-title)
  - [4.1. step 2.1: Title](#41-step-21-title)
    - [4.1.1. strategy/methods](#411-strategymethods)
    - [4.1.2. Results](#412-results)
- [5. Synthesis (optionnal): can be combining results from steps 1 and step 2](#5-synthesis-optionnal-can-be-combining-results-from-steps-1-and-step-2)
- [6. programs and dependencies](#6-programs-and-dependencies)

<!-- /TOC -->


## 1. Goal


## 2. Format of the result file(s) (optionnal)


## 3. Step 1

### 3.1. Step 1.1

#### 3.1.1. Strategy/methods

#### 3.1.2. Results

### 3.2. step 1.2: epidemiology dynamics

#### 3.2.1. strategy/methods

#### 3.2.2. Results


## 4. Step 2: Title

### 4.1. step 2.1: Title

#### 4.1.1. strategy/methods

#### 4.1.2. Results


## 5. Synthesis (optionnal): can be combining results from steps 1 and step 2

## 6. programs and dependencies
This part is essential for reproductibility and creation of the  singularity images, especially if analyses have been performed on a remote server (e.g. on a Galaxy server)
Here should be listed the informatics environment needed to perform the analysis:
- the global environment/OS: windows, linux, job submission to a Galaxy server, ...
- programs and dependencies, e.g.:
  - bash
  - GMAP version 2019-09-12
  - sort-bed 2.4.35
  - perl 5.30
  - gff2bed 2.4.35
  - BLAST xxx
  - R 3.6.3 (2020-02-29), with packages:
    - ggplot
    - X
    - Y
    - Z
  - Python 3.8.2, with packages:
    - ...
    - ...
  - anything else

[//]: # (link definitions)
[bed file link 2]: https://genome.ucsc.edu/FAQ/FAQformat.html#format1
